<?php
namespace App\City;
use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;
use PDO;


class City extends DB{
    public $id= "";
    public $name= "";
    public $city_name= "";


    public function __construct(){
        parent::__construct();
        if (!isset($_SESSION)) session_start();
    }

    public function setData($post = NULL){
        if (array_key_exists('id', $post)){
            $this->id= $post['id'];
        }
        if (array_key_exists('name', $post)){
            $this->name = $post['name'];
        }
        if (array_key_exists('city_name', $post)){
            $this->city_name = $post['city_name'];
        }

    }
    public function store(){
        $arrData = array( $this->name, $this->city_name);

        $sql = "Insert INTO city(name, city_name) VALUES (?,?)";
        $STH = $this->DBH->prepare($sql);

        $result = $STH->execute($arrData);

        if($result)
            Message::message("Success! Data Has Been Inserted Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Inserted Successfully :(");


        Utility::redirect('create.php');

    }



    public function index($fetchMode = 'ASSOC')
    {

        $STH = $this->DBH->query('SELECT * from city where is_deleted = \'No\'');

        $fetchMode = strtoupper($fetchMode);
        if (substr_count($fetchMode, 'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData = $STH->fetchAll();
        return $arrAllData;

    }
// end of index();
    public function view($fetchMode = 'ASSOC'){
        $sql= 'SELECT * from city WHERE id =' .$this->id;


        $STH = $this->DBH->query($sql);

        $fetchMode = strtoupper($fetchMode);
        if (substr_count($fetchMode, 'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arroneData = $STH->fetch();
        return $arroneData;

    }
// end of view();

    public function update(){
        // UPDATE `atomic_project_b35`.`book_title` SET `book_title` = 'nisi umm', `author_name` = 'nisi rtg t' WHERE `book_title`.`id` = 4;
        $arrData= array($this->name, $this->city_name);
        $sql= "Update city SET name = ?, city_name = ?  WHERE id =". $this->id;
        $STH= $this->DBH->prepare($sql);
        $STH-> execute($arrData);
        Utility::redirect('index.php');


    }
    // end of update();
    public function delete(){
        $sql= "DELETE FROM city WHERE id =". $this->id;
        //$sql= "DELETE FROM book_title WHERE id =". $this->id;
        $STH= $this->DBH->prepare($sql);
        $STH-> execute();
        Utility::redirect('index.php');


    }
    // end of delete



    public function trash(){

        $sql = "Update city SET is_deleted=NOW() where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $STH->execute();

        Utility::redirect('index.php');


    }// end of trash()



    public function trashed($fetchMode='ASSOC'){
        $sql = "SELECT * from city where is_deleted <> 'No' ";
        $STH = $this->DBH->query($sql);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of trashed();





    public function recover(){

        $sql = "Update city SET is_deleted='No' where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $STH->execute();

        Utility::redirect('index.php');

    }// end of recover();



}

// end of city Class

?>

