<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;

if(!isset( $_SESSION)) session_start();
echo Message::message();

?>


<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Hobbies - Create Form</title>

        <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
        <link rel="stylesheet" href="../../../resource/assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="../../../resource/assets/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="../../../resource/assets/css/form-elements.css">
        <link rel="stylesheet" href="../../../resource/assets/css/style.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="../../../resource/assets/js/html5shiv.js"></script>
            <script src="../../../resource/assets/js/respond.min.js"></script>
        <![endif]-->

        <!-- Favicon and touch icons -->
        <link rel="shortcut icon" href="../../../resource/assets/ico/favicon.png">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../../../resource/assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../../../resource/assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../../../resource/assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="../../../resource/assets/ico/apple-touch-icon-57-precomposed.png">

    </head>

    <body>
    <style>
        .main{
            margin-top: 10%;
            margin-left: 15%;
            margin-right:15%;
            background-color: #9f3d3a;



        }
        body{
            background-image:url("../../../resource/assets/img/backgrounds/Best-Hobbies-For-Men.jpg");

            background-repeat:no-repeat;
            background-size: 100% 925px;

        }



    </style>
        <!-- Top content -->
        <div class="top-content">
        	

                <div class="container">
       

                        <div class="col-sm-6 col-sm-offset-3 form-box">
                        	<div class="form-top">
                        		<div class="form-top-left">
                        			<h2><strong>Hobbies</strong></h2>
                            		<p><strong>Enter Hobbies :</strong></p>
                        		</div>
                        		<div class="form-top-right">
                        			<i class="fa fa-soccer-ball-o"></i>
                        		</div>
                            </div>
                            <div class="form-bottom">
			                    <form role="form" action="store.php" method="post" class="login-form">
			                    	<div class="form-group">
			                    		<label class="sr-only" for="form-hobbies">Name</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>

                                            <input type="text" name="name" placeholder="Name..." class="form-username form-control" id="form-username">
			                        </div>
                                        </div>

                                        <div class="form-group-lg">
                                            <label class="sr-only" for="pwd">Hobby</label>
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-plane"></i></span>

                                                <input type="checkbox" name="hobbies[]" value="Cooking">Cooking
                                            <input type="checkbox" name="hobbies[]" value="Painting">Painting</br>
                                            <input type="checkbox" name="hobbies[]" value="Travelling">Traveling
                                            <input type="checkbox" name="hobbies[]" value="Movies">Movies</br>
                                            <input type="checkbox" name="hobbies[]" value="Dancing">Dancing
                                            <input type="checkbox" name="hobbies[]" value="Coding">Coding</br>
                                            <input type="checkbox" name="hobbies[]" value="Driving">Driving
                                            <input type="checkbox" name="hobbies[]"  value="Gardening">Gardening</br>
                                            <input type="checkbox" name="hobbies[]" value="Singing">Singing
                                            <input type="checkbox" name="hobbies[]" value="Reading">Reading
                                           </div>
                                        </div>
			                        <button type="submit" class="btn">Create</button>
                                    <button type="reset" class="btn">Reset</button>
			                    </form>
		                    </div>
                        </div>


                </div>
            </div>
            



        <!-- Javascript -->
        <script src="../../../resource/assets/js/jquery-1.11.1.min.js"></script>
        <script src="../../../resource/assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="../../../resource/assets/js/jquery.backstretch.min.js"></script>
        <script src="../../../resource/assets/js/scripts.js"></script>
       
    </body>

</html>